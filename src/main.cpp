#include "mbed.h"
#include "ble/BLE.h"
#include "LedService.h"

/*
    Használat:
     - az eszköz neve 'h344409'
     - CURRENT TIME service-ként van regisztrálva
     - a karakterisztika azt jelzi, hogy mennyi időt (ms) várjon az eszköz két LED villogás között
    
    Megvalósítás:
     - a 'ledSpeed' változó tárolja a fenti időtartamot
     - az időzítést 'ticker' vezérli
     - új érték esetén a ticker módosul, hogy az új sebességgel villogtassa a LED-et
*/


DigitalOut led1(LED1, 0);
Serial usb(USBTX, USBRX);
char blue_device_name[] = "h344409";

unsigned ledSpeed = 200;

Ticker ticker;
EventQueue queue;
LedService* pLedService;



void blinkLed()
{
    led1 = !led1;
}

void blue_connected(const Gap::ConnectionCallbackParams_t* params)
{
    usb.printf("[blue] connected\r\n");
}

void blue_disconnected(const Gap::DisconnectionCallbackParams_t* params)
{
    usb.printf("[blue] disconnected\r\n");
    BLE::Instance().gap().startAdvertising();
}

void blue_init_completed(BLE::InitializationCompleteCallbackContext* params)
{
    BLE& ble = params->ble;
    ble_error_t error = params->error;

    if (error != BLE_ERROR_NONE)
    {
        /* In case of error, do sg meaningful */
        return;
    }

    /* Ensure that it is the default instance of BLE */
    if (ble.getInstanceID() != BLE::DEFAULT_INSTANCE)
        return;

    usb.printf("[blue] registering connection callbacks\r\n");
    ble.gap().onConnection(blue_connected);
    ble.gap().onDisconnection(blue_disconnected);
    
    /* Setup led service */
    pLedService = new LedService(ble);

    /* Setup advertising */
    usb.printf("[blue] configuring and starting advertising\r\n");

    uint16_t uuid16_list[] =
    {
        GattService::UUID_HEART_RATE_SERVICE,
        GattService::UUID_CURRENT_TIME_SERVICE  // TODO!
    };

    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::BREDR_NOT_SUPPORTED | GapAdvertisingData::LE_GENERAL_DISCOVERABLE);
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LIST_16BIT_SERVICE_IDS,
                                           (const uint8_t*)uuid16_list, sizeof(uuid16_list));
    ble.gap().accumulateAdvertisingPayload(GapAdvertisingData::COMPLETE_LOCAL_NAME,
                                     (const uint8_t*)blue_device_name, sizeof(blue_device_name));
    ble.gap().setAdvertisingType(GapAdvertisingParams::ADV_CONNECTABLE_UNDIRECTED);
    ble.gap().setAdvertisingInterval(1000); /* multiples of 0.625ms */
    ble.gap().startAdvertising();
}

void blue_process_events(BLE::OnEventsToProcessCallbackContext* context)
{
    queue.call(Callback<void()>(&BLE::Instance(), &BLE::processEvents));
}

int main()
{
    ticker.attach(blinkLed, ledSpeed / 1000.0f);

    BLE &ble = BLE::Instance();
    ble.onEventsToProcess(blue_process_events);
    ble.init(blue_init_completed);

    usb.printf("[main] program startup\r\n");
    queue.dispatch_forever();
    
    return 0;
}
