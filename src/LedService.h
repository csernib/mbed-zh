#ifndef CURRENTTIMESERVICE_H
#define CURRENTTIMESERVICE_H

#include "mbed.h"
#include "ble/BLE.h"

// Csúnya forward deklarációk...
extern DigitalOut led1;
extern Serial usb;
extern unsigned ledSpeed;
extern Ticker ticker;
extern void blinkLed();

class LedService
{
public:
    LedService(BLE &ble)
        : rMyBle(ble)
        , myLedCharacteristics(GattCharacteristic::UUID_CURRENT_TIME_CHAR /*TODO*/,
                               (uint8_t*)&ledSpeed,
                               sizeof(ledSpeed), sizeof(ledSpeed),
                               GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_READ |
                               GattCharacteristic::BLE_GATT_CHAR_PROPERTIES_WRITE)
    {
        myLedCharacteristics.setReadAuthorizationCallback(this, &LedService::readAuthorizationCallback);

        GattCharacteristic* charTable[] = { &myLedCharacteristics };
        GattService ledService(GattService::UUID_CURRENT_TIME_SERVICE /*TODO*/, charTable, sizeof(charTable) / sizeof(GattCharacteristic*));

        ble.addService(ledService);
        ble.onDataWritten(this, &LedService::onDataWritten);
    }

protected:
    void onDataWritten(const GattWriteCallbackParams* params)
    {
        if (params->handle == myLedCharacteristics.getValueAttribute().getHandle())
        {
            memcpy(&ledSpeed, params->data, sizeof(ledSpeed));
            ticker.detach();
            ticker.attach(blinkLed, ledSpeed / 1000.0f);
        }
    }

    void readAuthorizationCallback(GattReadAuthCallbackParams *params)
    {
        // See GattCharacteristic::authorizeRead
        // "If the read is approved, a new value can be provided by setting the params->data pointer and params->len fields."
        params->data = (uint8_t*)&ledSpeed;
        params->len = sizeof(ledSpeed);
        params->authorizationReply = AUTH_CALLBACK_REPLY_SUCCESS;
    }

    BLE &rMyBle;
    GattCharacteristic myLedCharacteristics;
};

#endif /* CURRENTTIMESERVICE_H */
